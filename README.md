<!-- {
	"name": "KompakktCacheCleaner",
	"author": "Lukas Günther",
	"url": "https://gitlab.com/nfdi4culture/ta1-data-enrichment/wbextension-kompakkt-update-cache",
	"description": "Send a signal to Kompakkt on every item update to clear the cache",
	"version": "0.1",
	"license-name": "MIT",
	"type": "other",
	"manifest_version": 2,
	"AutoloadClasses": {
		"MultiContentHook": "src/MultiContentHook.php"
	},
	"Hooks": {
		"MultiContentSave": "MultiContentHook::onMultiContentSave"
	},
	"config": {
		"KompakktSecret": {
			"description": "Kompakkt secret",
			"value": "bWoCbyDoj!&snNq4"
		},
		"KompakktPath": {
			"description": "Kompakkt path",
			"value": "http://172.22.0.14:8080"
		}
	},
	"ConfigRegistry": {
		"KompakktCacheCleaner": "GlobalVarConfig::newInstance"
	}
}


Generate Readme from above .json: -->

# KompakktCacheCleaner

Send a signal to Kompakkt on every item update to clear the cache

## Installation

### Requirements

* PHP 7.0 or higher

### Download

* Download the latest release from [here](https://gitlab.com/nfdi4culture/ta1-data-enrichment/wbextension-kompakkt-update-cache/-/releases).
* Extract the files in a directory called `KompakktCacheCleaner` in your `extensions` folder.

### Enable

Add the following code at the bottom of your `LocalSettings.php` file:

```php
wfLoadExtension( 'KompakktCacheCleaner' );
```

### Configuration

After installation, you need to configure the extension. You can do this by adding the following code to your `LocalSettings.php` file:

```php
$wgKompakktSecret = "your-secret";
$wgKompakktPath = "http://<kompakkt-ip>:8080";
```

## License

KompakktCacheCleaner is licensed under the [MIT license](https://opensource.org/licenses/MIT).
