<?php

use MediaWiki\MediaWikiServices;

class MultiContentHook {
    public static function onMultiContentSave ( 
        MediaWiki\Revision\RenderedRevision $renderedRevision, 
        MediaWiki\User\UserIdentity $user, 
        CommentStoreComment $summary, 
        $flags, 
        Status $hookStatus 
        ): void
        {
        // get secret from config
        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig( 'KompakktCacheCleaner' );
        $secret = $config->get("KompakktSecret");
        // get kompakkt path from config
        $kompakktPath = $config->get("KompakktPath");

        // post secret to /utility/rebuildcache
        $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n",
            'content' => json_encode(array("secret" => $secret))
            )
        );
        $context = stream_context_create($context_options);
        $result = file_get_contents($kompakktPath . "/utility/rebuildcache", false, $context);


        //if response is not 200, write to the debug log
        if ($http_response_header[0] != "HTTP/1.1 200 OK") {
            wfDebugLog("KompakktCacheCleaner", "KompakktCacheCleaner: " . $http_response_header[0] . " || Error: " . $result);
        }
    }
}